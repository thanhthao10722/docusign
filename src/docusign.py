import docusign_esign as docusign
import base64
import config as config


def generate_access_token():
    api_client = docusign.ApiClient(config.BASE_URL)
    api_client.oauth_host_name = config.OAUTH_HOST_NAME
    token_obj = api_client.request_jwt_user_token(
        client_id=config.CLIENT_ID,
        user_id=config.USER_ID,
        oauth_host_name=config.OAUTH_HOST_NAME,
        private_key_bytes=config.PRIVATE_KEY,
        expires_in=36000
    )
    return token_obj


def send_document_for_signing(signer_name, signer_email):
    access_token = generate_access_token().access_token
    document_b64 = base64.b64encode(
        bytes(create_document(signer_name, signer_email),
              'utf-8')).decode('ascii')

    document = docusign.Document(
        document_base64=document_b64,
        name='Document',
        file_extension='html',
        document_id=1
    )
    signer = docusign.Signer(
        email=signer_email,
        name=signer_name, recipient_id="1", routing_order="1")

    sign_here = docusign.SignHere(
        document_id='1',
        page_number='1', recipient_id='1', tab_label='SignHere',
        x_position='0', y_position='0')

    signer.tabs = docusign.Tabs(sign_here_tabs=[sign_here])

    envelope_definition = docusign.EnvelopeDefinition(
        email_subject="Please sign this document",
        documents=[document],
        recipients=docusign.Recipients(signers=[signer]),
        status="sent"
    )

    api_client = docusign.ApiClient(config.BASE_URL)
    api_client.set_default_header("Authorization", "Bearer " + access_token)

    envelope_api = docusign.EnvelopesApi(api_client)
    envelope = envelope_api.create_envelope(
        config.ACCOUNT_ID, envelope_definition=envelope_definition)
    return envelope


def create_document(signer_name, signer_email):
    return f"""
        <!DOCTYPE html>
        <html>
            <head>
              <meta charset="UTF-8">
            </head>
            <body style="font-family:sans-serif;margin-left:2em;">
            <h1 style="font-family: 'Trebuchet MS', Helvetica, sans-serif;
                color: darkblue;margin-bottom: 0;">World Wide Corp</h1>
            <h2 style="font-family: 'Trebuchet MS', Helvetica, sans-serif;
              margin-top: 0px;margin-bottom: 3.5em;font-size: 1em;
              color: darkblue;">Order Processing Division</h2>
            <h4>Ordered by {signer_name}</h4>
            <p style="margin-top:0em; margin-bottom:0em;">Email:{signer_email}</p>
            <p style="margin-top:3em;">
      Candy bonbon pastry jujubes lollipop wafer biscuit biscuit. Topping brownie sesame snaps sweet roll pie. Croissant danish biscuit soufflé caramels jujubes jelly. Dragée danish caramels lemon drops dragée. Gummi bears cupcake biscuit tiramisu sugar plum pastry. Dragée gummies applicake pudding liquorice. Donut jujubes oat cake jelly-o. Dessert bear claw chocolate cake gummies lollipop sugar plum ice cream gummies cheesecake.
            </p>
            <!-- Note the anchor tag for the signature field is in white. -->
            <h3 style="margin-top:3em;">Agreed: <span style="color:white;">**signature_1**/</span></h3>
            </body>
        </html>
      """


if __name__ == "__main__":
    print(send_document_for_signing(
        'Thanh Thao', 'thanhthao120722@gmail.com'))
